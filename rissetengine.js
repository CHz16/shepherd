// rissetengine.js
// Engine to manage a bunch of positionally situated sound sources generating Risset scales (continuous Shepard scales).
// The engine will start playing immediately after it's created.

function RissetEngine(sources, args) {
    function RissetSource(args) {
        this.setGains = function() {
            //document.getElementById("debug").innerHTML = "";
            for (let i = 0; i < this.gains.length; i++) {
                // What we want is to take the frequency and calculate how far it is between the lowest and highest possible frequencies in the range, expressed as a number between 0 and 1 which we'll call x. Since the frequency curve is exponential, we need a logarithm.
                let f = this.oscillators[i].frequency.value;
                let x = Math.log2(f / (this.centerFrequency / Math.pow(2, (2 * this.octaveSpan + 1) / 2))) / (2 * this.octaveSpan + 1);

                // Sine half-wave centered at this.centerFrequency
                this.gains[i].gain.value = Math.sin(x * Math.PI) * this.maxGain;
                //document.getElementById("debug").innerHTML += Math.round(f) + " " + this.gains[i].gain.value + "<br />";
            }
        }

        this.movePanner = function(panner, x, y) {
            this.panners[panner].x = x;
            this.panners[panner].y = y;

            if (this.panners[panner].positionX !== undefined) {
                this.panners[panner].positionX.value = x;
                this.panners[panner].positionY.value = y;
                this.panners[panner].positionZ.value = 15;
            } else {
                this.panners[panner].setPosition(x, y, 15);
            }
        }

        this.movePanners = function(newX, newY, newListenerX, newListenerY) {
            // Find the closest panner to the listener.
            let closestPanner, closestPannerDistance = gameWidth * gameWidth + gameHeight * gameHeight;
            for (let i = 0; i < this.panners.length; i++) {
                let d = distanceSquared(this.listenerX, this.listenerY, this.panners[i].x, this.panners[i].y);
                if (d < closestPannerDistance) {
                    closestPanner = i;
                    closestPannerDistance = d;
                }
            }

            this.x = newX;
            this.y = newY;
            this.listenerX = newListenerX;
            this.listenerY = newListenerY;

            // Calculate the new positions of the panners and find which new one will be closest to the player.
            let xDir = (newX < newListenerX) ? 1 : -1;
            let yDir = (newY < newListenerY) ? 1 : -1;
            let newPanners = [
                                [newX, newY],
                                [newX + xDir * gameWidth, newY],
                                [newX, newY + yDir * gameWidth],
                                [newX + xDir * gameWidth, newY + yDir * gameWidth]
                             ];

            let closestNewPanner, closestNewPannerDistance = gameWidth * gameWidth + gameHeight * gameHeight;
            for (let i = 0; i < newPanners.length; i++) {
                let d = distanceSquared(newListenerX, newListenerY, newPanners[i][0], newPanners[i][1]);
                if (d < closestNewPannerDistance) {
                    closestNewPanner = i;
                    closestNewPannerDistance = d;
                }
            }

            // Move the panners, reallocating them in such an order that the panner that WAS closest to the listener is the same panner that's now closest to the listener in the new positions.
            let offset = (closestNewPanner - closestPanner + 4) % 4;
            for (let i = 0; i < this.panners.length; i++) {
                let i2 = (i - offset + 4) % 4;
                this.movePanner(i2, newPanners[i][0], newPanners[i][1]);
            }
        }


        // Save the arguments
        args = args || new Object();
        let x = args.x || 0;
        let y = args.y || 0;
        this.listenerX = 0;
        this.listenerY = 0;
        if (args.bottomFrequency !== undefined) {
            this.bottomFrequency = args.bottomFrequency;
            this.centerFrequency = args.bottomFrequency * Math.sqrt(2);
        } else if (args.centerFrequency !== undefined) {
            this.bottomFrequency = args.centerFrequency / Math.sqrt(2);
            this.centerFrequency = args.centerFrequency;
        } else {
            this.bottomFrequency = 440;
            this.centerFrequency = 440 * Math.sqrt(2);
        }
        this.maxGain = args.maxGain || 0.025;
        this.octaveSpan = (args.octaveSpan !== undefined) ? args.octaveSpan : 3;
        let waveType = args.type || "sine";

        // Set up the graph
        // Each oscillator feeds into its own gain node, because their volumes need to be set separately. Each of those gains then get sent into one master gain, which is used to add their signals together for the panners. The combined signal is then sent into FOUR panners, one at the location of the source and three other "virtual" sources to simulate the sound wrapping around the screen, as the world is toroidal.
        this.masterGain = audioContext.createGain();
        this.masterGain.gain.value = 0;
        this.panners = [];
        for (let i = 0; i < 4; i++) {
            let panner = audioContext.createPanner();
            this.panners.push(panner);
            this.masterGain.connect(panner);
            panner.connect(audioContext.destination);

            if (panner.orientationX !== undefined) {
                panner.orientationX.value = 0;
                panner.orientationY.value = 0;
                panner.orientationZ.value = -1;
            } else {
                panner.setOrientation(0, 0, -1);
            }
            panner.refDistance = 30;
            panner.rolloffFactor = 3;
            panner.panningModel = "HRTF";
            panner.distanceModel = "exponential";
            panner.x = 0;
            panner.y = 0;
        }

        this.oscillators = [], this.gains = [];
        for (let i = 0; i < (2 * this.octaveSpan + 1); i++) {
            let oscillator = audioContext.createOscillator();
            this.oscillators.push(oscillator);
            if (args.waveform === undefined) {
                oscillator.type = waveType;
            } else {
                oscillator.setPeriodicWave(args.waveform);
            }

            let gain = audioContext.createGain();
            this.gains.push(gain);

            oscillator.connect(gain);
            gain.connect(this.masterGain);
        }
        this.setPosition(x, y);
        this.setScalePosition(args.startScalePosition || 0);

        // Start soundin'
        for (let i = 0; i < (2 * this.octaveSpan + 1); i++) {
            this.oscillators[i].start();
        }
    }

    RissetSource.prototype.setPosition = function(x, y) {
        this.movePanners(x, y, this.listenerX, this.listenerY);
    }

    RissetSource.prototype.setPositionOfListener = function(x, y) {
        this.movePanners(this.x, this.y, x, y);
    }

    RissetSource.prototype.setScalePosition = function(scalePosition) {
        // Find the desired frequency from the scale position.
        let frequency = this.bottomFrequency * Math.pow(2, scalePosition);

        // To keep looping smooth, we don't want to change the frequency of every oscillator super drastically. So, we'll find the oscillator that currently has the frequency closest to what we want to change the main frequency to and use that as the pivot.
        let closestOscillator = 0, minDifference = Math.abs(this.oscillators[0].frequency.value - frequency);
        for (let i = 1; i < this.oscillators.length; i++) {
            let difference = Math.abs(this.oscillators[i].frequency.value - frequency);
            if (difference < minDifference) {
                closestOscillator = i;
                minDifference = difference;
            }
        }

        // The closest oscillator will be the one in the middle of the new range, but we want the loop that sets the frequencies to start from the beginning of the range, so we need to adjust the index.
        // Addition here instead of subtraction works fine because of modulo math, and is slightly preferable because it'll avoid any accidentally negative numbers.
        closestOscillator += this.octaveSpan + 1;

        // Now we can set the frequencies.
        for (let i = 0; i < this.oscillators.length; i++) {
            let offsetIndex = (i + closestOscillator) % this.oscillators.length;
            this.oscillators[offsetIndex].frequency.value = frequency * Math.pow(2, i - this.octaveSpan);
        }

        this.setGains();
    }

    RissetSource.prototype.mute = function() {
        this.masterGain.gain.value = 0;
    }

    RissetSource.prototype.unmute = function() {
        this.masterGain.gain.value = 1;
    }

    RissetSource.prototype.stop = function() {
        for (let i = 0; i < this.oscillators.length; i++) {
            this.oscillators[i].stop();
        }
        for (let i = 0; i < this.panners.length; i++) {
            this.panners[i].disconnect(audioContext.destination);
        }
        this.oscillators = [];
        this.gains = [];
        this.masterGain = undefined;
        this.panners = [];
    }


    // Read in the arguments
    args = args || new Object();
    this.listenerX = args.listenerX || 0;
    this.listenerY = args.listenerY || 0;

    // Set up the internal data structures
    this.sources = [];
    if (typeof sources == "number") {
        for (let i = 0; i < sources; i++) {
            this.sources.push(new RissetSource());
        }
    } else {
        for (let i = 0; i < sources.length; i++) {
            this.sources.push(new RissetSource(sources[i]));
        }
    }
    this.setPositionOfListener(this.listenerX, this.listenerY);
    this.isPlaying = false;
}


RissetEngine.prototype.setPositionOfSource = function(source, x, y) {
    this.sources[source].setPosition(x, y);
}

RissetEngine.prototype.setPositionOfListener = function(x, y) {
    this.listenerX = x;
    this.listenerY = y;

    if (audioContext.listener.positionX !== undefined) {
        audioContext.listener.positionX.value = x;
        audioContext.listener.positionY.value = y;
    } else {
        audioContext.listener.setPosition(x, y, 0);
    }

    for (let i = 0; i < this.sources.length; i++) {
        this.sources[i].setPositionOfListener(x, y);
    }
}

RissetEngine.prototype.setScalePositionOfSource = function(source, scalePosition) {
    this.sources[source].setScalePosition(scalePosition);
}

// Start sounding all the sources. If the engine is already unmuted, then this does nothing.
RissetEngine.prototype.unmute = function() {
    if (this.isPlaying) {
        return;
    }

    for (let i = 0; i < this.sources.length; i++) {
        this.sources[i].unmute();
    }
    this.isPlaying = true;
}

// Stop sounding all the sources. If the engine is already muted, then this does nothing.
RissetEngine.prototype.mute = function() {
    if (!this.isPlaying) {
        return;
    }

    for (let i = 0; i < this.sources.length; i++) {
        this.sources[i].mute();
    }
    this.isPlaying = false;
}

// Stop sounding all the sources and then destroys them. Use this when you're through with the engine and want it gone.
RissetEngine.prototype.stop = function() {
    for (let i = 0; i < this.sources.length; i++) {
        this.sources[i].stop();
    }
    this.sources = [];
    this.isPlaying = false;
}
