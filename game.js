// game.js
// Shepard main game code.


//
// GAME PARAMETERS
//

var gameWidth = 500, gameHeight = 500;
var backgroundColor = "white";
var frameColor = "black", frameWidth = 2;

var irisDuration = 500; // milliseconds

var minBodies = 1, maxBodies = 10;


//
// GLOBALS
//

var audioContext;
var canvas, canvasContext;
var offscreenCanvas, offscreenCanvasContext;

var isLoaded = false;
var scenes = [], currentScene = 0; // 0 = title, 1 = game, 2 = test

var isPaused = false, pauseTime;
var currentTransitionType, transitionStartTime, deferredDestination;

var defaults = new JSUserDefaults("shepherd_", {
    sound: "yes",
    numberOfBodies: 2,
    freePlay: "no",
    highScores: Array(maxBodies - minBodies + 1).fill(0)
});


//
// INPUT
//

var Key = {up: 0, down: 1, left: 2, right: 3, action: 4, cancel: 5};

var keyBindings = [];
keyBindings[38] = Key.up;
keyBindings[40] = Key.down;
keyBindings[37] = Key.left;
keyBindings[39] = Key.right;
keyBindings[90] = Key.action;
keyBindings[88] = Key.cancel;

var keyDown = [];


//
// INITIALIZATION
//

window.onload = init;
function init() {
    // Migrate data from cookies to localStorage
    let cookieKeys = ["sound", "numberOfBodies", "freePlay", "highScores"], cookieValues = [], needsMigration = false;
    let cookiePairs = document.cookie.split("; ");
    for (let i = 0; i < cookiePairs.length; i++) {
        let cookiePair = cookiePairs[i].split("=");
        if (cookieKeys.indexOf(cookiePair[0]) !== -1) {
            needsMigration = true;
            cookieValues[cookiePair[0]] = JSON.parse(decodeURIComponent(cookiePair[1]));
        }
    }
    if (needsMigration) {
        let thePast = new Date();
        thePast.setTime(thePast.getTime() - 100000000);
        thePast = thePast.toGMTString();
        for (let i = 0; i < cookieKeys.length; i++) {
            if (cookieValues[cookieKeys[i]] !== undefined) {
                defaults.set(cookieKeys[i], cookieValues[cookieKeys[i]]);
                document.cookie = cookieKeys[i] + "=; domain=" + document.domain + "; path=/; expires=" + thePast;
            }
        }
    }


    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    canvas = canvasThings.canvas;
    canvasContext = canvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.body.replaceChild(canvas, document.getElementById("canvas"));
    canvas.style.width = gameWidth + "px";
    canvas.style.height = gameHeight + "px";

    let offscreenCanvasThings = makeScaledCanvas(gameWidth, gameHeight);
    offscreenCanvas = offscreenCanvasThings.canvas;
    offscreenCanvasContext = offscreenCanvasThings.context;

    // Set up canvas events for focusing and grabbing input and all that
    canvas.tabIndex = 0;
    canvas.addEventListener("keydown", keyDownHandler);
    canvas.addEventListener("keyup", keyUpHandler);
    canvas.addEventListener("focus", function() {
        // Defer setting up the audio context until the user has clicked the game, for Chrome autoplay restrictions
        if (audioContext === undefined) {
            audioContext = new (window.AudioContext || window.webkitAudioContext)();
        }

        if (isLoaded && isPaused) {
            isPaused = false;
            scenes[currentScene].unpause(pauseTime);
            if (deferredDestination !== undefined) {
                scenes[deferredDestination].unpause(pauseTime);
            }
            window.requestAnimationFrame(draw);
        }
    });
    canvas.addEventListener("blur", function(event) { canvas.blur(); });

    // Construct all the scenes now
    scenes.push(new TitleSceneController());
    scenes.push(new GameSceneController());
    if (typeof TestSceneController !== "undefined") {
        scenes.push(new TestSceneController());
    }

    // Load the first scene and kick off the draw loop
    isLoaded = true;
    scenes[currentScene].startTransitionIn(Transition.immediate);
    scenes[currentScene].finishTransitionIn(Transition.immediate);
    window.requestAnimationFrame(draw);
}


//
// DRAWING
//

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

function draw(timestamp) {
    // Update the start time of the transition if we're unpausing.
    if (pauseTime !== undefined) {
        transitionStartTime += timestamp - pauseTime;
        pauseTime = undefined;
    }

    // Update the current transition, if we're in the middle of one.
    if (currentTransitionType !== undefined) {
        if (timestamp >= (transitionStartTime + irisDuration)) {
            finishTransition();
        }
    }

    // Clear the background and tell the current scene to draw
    canvasContext.fillStyle = backgroundColor;
    canvasContext.fillRect(0, 0, gameWidth, gameHeight);
    scenes[currentScene].draw(canvasContext, timestamp);

    // If we're transitioning, then draw the new scene on top of the old one, clipped as necessary
    if (currentTransitionType !== undefined) {
        // Clear the background of the offscreen context and tell the new scene to draw there
        offscreenCanvasContext.fillStyle = backgroundColor;
        offscreenCanvasContext.fillRect(0, 0, gameWidth, gameHeight);
        scenes[deferredDestination].draw(offscreenCanvasContext, timestamp);

        // Create the mask depending on how we're transitioning
        canvasContext.save();
        let mask;
        canvasContext.beginPath();
        if (currentTransitionType == Transition.irisIn) {
            let radius = (1 - (timestamp - transitionStartTime) / irisDuration) * Math.sqrt(gameWidth * gameWidth + gameHeight * gameHeight) / 2;

            canvasContext.rect(0, 0, gameWidth, gameHeight);
            canvasContext.arc(gameWidth / 2, gameHeight / 2, radius, 0, 360);
        } else if (currentTransitionType == Transition.irisOut) {
            let radius = ((timestamp - transitionStartTime) / irisDuration) * Math.sqrt(gameWidth * gameWidth + gameHeight * gameHeight) / 2;

            canvasContext.arc(gameWidth / 2, gameHeight / 2, radius, 0, 360);
        }
        canvasContext.closePath();
        canvasContext.clip("evenodd");

        // Now we can draw the sucker
        canvasContext.drawImage(offscreenCanvas, 0, 0, gameWidth, gameHeight);
        canvasContext.restore();
    }

    // Stroke the game border
    canvasContext.strokeStyle = frameColor;
    canvasContext.lineWidth = frameWidth;
    canvasContext.strokeRect(frameWidth / 2, frameWidth / 2, gameWidth - frameWidth, gameHeight - frameWidth);

    if (document.activeElement != canvas) {
        // If the canvas has lost focus, then pause the game.
        isPaused = true;
        pauseTime = timestamp;
        scenes[currentScene].pause(pauseTime);
        if (deferredDestination !== undefined) {
            scenes[deferredDestination].pause(pauseTime);
        }

        // Darken the last-drawn frame
        canvasContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        canvasContext.fillRect(0, 0, gameWidth, gameHeight);

        // Plop a pause message on top of that
        canvasContext.font = "32px sans-serif";
        let metrics = canvasContext.measureText("(click to continue)");
        let x = Math.round((gameWidth - metrics.width) / 2);
        canvasContext.fillStyle = "#222222";
        canvasContext.fillRect(x - 10, 265, metrics.width + 20, 50);
        canvasContext.fillStyle = "white";
        canvasContext.fillText("(click to continue)", x, 300);
    } else {
        // Otherwise if we still have focus, then request another draw
        window.requestAnimationFrame(draw);
    }
}

// Draws a circle of a given radius at a given point in a given context. If the circle would be cut off by one of the edges, the remainder of the circle will be drawn on the other side.
function drawWrappingCircle(context, x, y, radius) {
    let xCoords = [x];
    if (x < radius) {
        xCoords.push(x + gameWidth);
    } else if (x > (gameWidth - radius)) {
        xCoords.push(x - gameWidth);
    }

    let yCoords = [y];
    if (y < radius) {
        yCoords.push(y + gameHeight);
    } else if (y > (gameHeight - radius)) {
        yCoords.push(y - gameHeight);
    }

    for (let i = 0; i < xCoords.length; i++) {
        for (let j = 0; j < yCoords.length; j++) {
            context.beginPath();
            context.arc(xCoords[i], yCoords[j], radius, 0, 360);
            context.closePath();
            context.fill();
        }
    }
}

// Draws text centered horizontally at the given y coordinate.
function drawCenteredText(context, text, y) {
    let metrics = context.measureText(text);
    let x = Math.round((gameWidth - metrics.width) / 2);
    context.fillText(text, x, y);
}


//
// SCENE TRANSITIONS
//

var Transition = {immediate: 0, irisIn: 1, irisOut: 2};

function startTransition(destination, transitionType) {
    scenes[currentScene].startTransitionOut(transitionType);
    scenes[destination].startTransitionIn(transitionType);

    currentTransitionType = transitionType;
    transitionStartTime = performance.now();
    deferredDestination = destination;
    if (currentTransitionType == Transition.immediate) {
        finishTransition();
    }
}

function finishTransition() {
    scenes[currentScene].finishTransitionOut(currentTransitionType);
    currentScene = deferredDestination;
    scenes[currentScene].finishTransitionIn(currentTransitionType);

    currentTransitionType = undefined;
    transitionStartTime = undefined;
    deferredDestination = undefined;
}


//
// INPUT HANDLERS
//

function keyDownHandler(event) {
    if (!isLoaded) {
        return;
    }

    let key = keyBindings[event.keyCode];
    keyDown[key] = true;
    if (currentTransitionType === undefined && key !== undefined) {
        scenes[currentScene].keyDown(key);
        event.preventDefault();
    }
}

function keyUpHandler(event) {
    if (!isLoaded) {
        return;
    }

    let key = keyBindings[event.keyCode];
    keyDown[key] = false;
    if (currentTransitionType === undefined && key !== undefined) {
        scenes[currentScene].keyUp(key);
        event.preventDefault();
    }
}

function sendToScene(key, timestamp) {
    clearPendingDirection();
    scenes[currentScene].keyDown(key, timestamp);
}


//
// ETC.
//

function distanceSquared(x1, y1, x2, y2) {
    return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
}

function distanceBetweenPoints(x1, y1, x2, y2) {
    return Math.sqrt(distanceSquared(x1, y1, x2, y2));
}
