// gamescene.js
// Controller for the game scene.

var GameState = {
    TransitioningIn: {moveIris: false, moveBodies: false, movePlayer: false},
    CountingDown: {moveIris: true, moveBodies: false, movePlayer: false},
    Playing: {moveIris: true, moveBodies: true, movePlayer: true},
    Hurt: {moveIris: true, moveBodies: true, movePlayer: true},
    FreePlaying: {moveIris: true, moveBodies: true, movePlayer: true},
    GameOver: {moveIris: true, moveBodies: true, movePlayer: true},
    Resetting: {moveIris: false, moveBodies: false, movePlayer: false},
    TransitioningOut: {moveIris: false, moveBodies: false, movePlayer: false},
    Null: {moveIris: false, moveBodies: false, movePlayer: false}
};

var gameCountdownDelay = 500, gameCountdownLength = 3000; // milliseconds
var gameDuration = 60 * 1000; // milliseconds
var gameHurtDuration = 1000; // milliseconds
var gameScreenShakeDuration = 500; // milliseconds, should <= gameHurtDuration
var gameScreenShakeMaxPerturbation = 10; // pixels
var gameFrequencyShakeMaxPerturbation = 0.5; // along the scale from 0 to 1
var gameResetDuration = 300; // milliseconds
var gameBodyMinimumGain = 0.025, gameBodyMaximumGain = 0.1;

function GameSceneController() {
    this.cloneBodies = function(bodies) {
        let clonedBodies = [];
        for (let i = 0; i < bodies.length; i++) {
            clonedBodies.push(Object.assign({}, bodies[i]));
        }
        return clonedBodies;
    }

    this.updateCallback = function(timeslice, collidedWithControlPoint) {
        // This is solely to find the "exact" time of the collision to calculate when the hurt period should stop during multiple invocations of this callback. After the bodies are all updated, lastUpdate will be set to the draw timestamp anyway.
        this.lastUpdate += timeslice;

        if (this.state != GameState.Playing) {
            return;
        }

        if (collidedWithControlPoint) {
            this.state = GameState.Hurt;
            this.gameHurtEndTime = this.lastUpdate + gameHurtDuration;
            return;
        }

        // So points. You score points constantly during the game, and the closer you are to things, the more points you get.
        // So for every body, we calculate how close you are to it, scaled down to a number between 0 and 1. 0 means the coordinates are the same, 1 means it's the maximum distance away (half the diagonal of the playing field. Then we add that, multiplied by the timeslice, to the score.
        // To be more accurate, we'd want to integrate along the paths things take. But the physics are already inaccurate because they don't integrate either, so I'm like ¯\_(ツ)_/¯
        let maximumDistance = Math.sqrt(gameWidth * gameWidth + gameHeight * gameHeight) / 2;
        for (let i = 0; i < this.gravitationalSystem.bodies.length; i++) {
            let virtualBodies = this.gravitationalSystem.makeVirtualBodies(
                this.gravitationalSystem.controlPoint,
                this.gravitationalSystem.bodies[i]
            );
            let d = distanceBetweenPoints(this.gravitationalSystem.controlPoint.x, this.gravitationalSystem.controlPoint.y, virtualBodies.virtualBody2.x, virtualBodies.virtualBody2.y);
            this.score += timeslice * (1 - d / maximumDistance);
        }
    }

    this.updateRissetEngine = function(shakeAmplitude) {
        shakeAmplitude = shakeAmplitude || 0;
        this.rissetEngine.setPositionOfListener(this.gravitationalSystem.controlPoint.x, this.gravitationalSystem.controlPoint.y);

        let frequencyShake = (Math.random() <= 0.5 ? 1 : -1) * Math.random() * shakeAmplitude * gameFrequencyShakeMaxPerturbation;
        for (let i = 0; i < this.gravitationalSystem.bodies.length; i++) {
            let body = this.gravitationalSystem.bodies[i];
            this.rissetEngine.setPositionOfSource(i, body.x, body.y);

            let scalePosition = (body.x / gameWidth + body.y / gameHeight + frequencyShake) % 1;
            this.rissetEngine.setScalePositionOfSource(i, scalePosition);
        }
    }


    this.gravitationalSystem = undefined;
    this.originalBodies = undefined, this.bodiesAtReset = undefined;
    this.lastUpdate = undefined;

    this.rissetEngine = undefined;

    this.score = 0;
    this.irisX = undefined, this.irisY = undefined;
    this.newHighScore = false;

    this.state = GameState.null;
    this.gameStartTime = undefined, this.gameEndTime = undefined;
    this.gameHurtEndTime = undefined;
    this.gameResetTime = undefined;
}

GameSceneController.prototype.draw = function(context, timestamp) {
    context.fillStyle = "black";
    context.fillRect(0, 0, gameWidth, gameHeight);

    // If the countdown is over, set some state to start the game.
    if (this.state == GameState.CountingDown && timestamp >= this.gameStartTime) {
        if (defaults.get("freePlay") == "yes") {
            this.state = GameState.FreePlaying;
        } else {
            this.state = GameState.Playing;
            this.gameEndTime = this.gameStartTime + gameDuration;
        }

        if (this.rissetEngine !== undefined) {
            this.rissetEngine.unmute();
        }

        this.lastUpdate = this.gameStartTime;
        this.gameStartTime = undefined;
    }

    // If the hurt period is over, then unhurt the player.
    if (this.state == GameState.Hurt && timestamp >= this.gameHurtEndTime) {
        this.state = GameState.Playing;
        this.gameHurtEndTime = undefined;
    }

    // If the game is over, update the state.
    if ((this.state == GameState.Playing || this.state == GameState.Hurt) && timestamp >= this.gameEndTime) {
        this.state = GameState.GameOver;

        // Check if we got a new high score
        this.score = Math.round(this.score);
        let highScores = defaults.get("highScores");
        let scoreIndex = defaults.get("numberOfBodies") - minBodies;
        if (this.score > highScores[scoreIndex]) {
            this.newHighScore = true;
            highScores[scoreIndex] = this.score;
            defaults.set("highScores", highScores);
        }

        this.gameHurtEndTime = undefined;
        this.gameEndTime = undefined;
    }

    // Tween the bodies back to their original positions when resetting
    if (this.state == GameState.Resetting) {
        if (timestamp >= this.gameResetTime) {
            this.state = GameState.CountingDown;

            this.gameStartTime = performance.now() + gameCountdownDelay + gameCountdownLength;

            for (let i = 0; i < this.originalBodies.length; i++) {
                this.gravitationalSystem.allBodies[i].x = this.originalBodies[i].x;
                this.gravitationalSystem.allBodies[i].y = this.originalBodies[i].y;
                this.gravitationalSystem.allBodies[i].xVelocity = this.originalBodies[i].xVelocity;
                this.gravitationalSystem.allBodies[i].yVelocity = this.originalBodies[i].yVelocity;
            }
            this.bodiesAtReset = undefined;
        } else {
            let progress = (this.gameResetTime - timestamp) / gameResetDuration;
            for (let i = 0; i < this.originalBodies.length; i++) {
                this.gravitationalSystem.allBodies[i].x = progress * this.bodiesAtReset[i].x + (1 - progress) * this.originalBodies[i].x;
                this.gravitationalSystem.allBodies[i].y = progress * this.bodiesAtReset[i].y + (1 - progress) * this.originalBodies[i].y;
            }
        }
    }

    // Check if the player is holding down in a direction.
    let movementVector = {x: 0, y: 0};
    if (keyDown[Key.left]) {
        movementVector.x -= 1;
    }
    if (keyDown[Key.right]) {
        movementVector.x += 1;
    }
    if (keyDown[Key.up]) {
        movementVector.y -= 1;
    }
    if (keyDown[Key.down]) {
        movementVector.y += 1;
    }
    movementVector.x *= 3;
    movementVector.y *= 3;
    if (movementVector.x != 0 && movementVector.y != 0) {
        movementVector.x /= Math.sqrt(2);
        movementVector.y /= Math.sqrt(2);
    }

    // Move the iris.
    if (this.state.moveIris) {
        let irisDestinationX = movementVector.x * 2;
        let irisDestinationY = movementVector.y * 2;
        if (irisDestinationX > this.irisX) {
            this.irisX = Math.min(this.irisX + 0.8, irisDestinationX);
        } else if (irisDestinationX < this.irisX) {
            this.irisX = Math.max(this.irisX - 0.8, irisDestinationX);
        }
        if (irisDestinationY > this.irisY) {
            this.irisY = Math.min(this.irisY + 0.8, irisDestinationY);
        } else if (irisDestinationY < this.irisY) {
            this.irisY = Math.max(this.irisY - 0.8, irisDestinationY);
        }
    }

    // Move the player.
    if (this.state.movePlayer) {
        this.gravitationalSystem.controlPoint.x = (this.gravitationalSystem.controlPoint.x + movementVector.x) % gameWidth;
        if (this.gravitationalSystem.controlPoint.x < 0) {
            this.gravitationalSystem.controlPoint.x += gameWidth;
        }
        this.gravitationalSystem.controlPoint.y = (this.gravitationalSystem.controlPoint.y + movementVector.y) % gameHeight;
        if (this.gravitationalSystem.controlPoint.y < 0) {
            this.gravitationalSystem.controlPoint.y += gameHeight;
        }
    }

    // Update the bodies
    if (this.state.moveBodies) {
        this.gravitationalSystem.update(timestamp - this.lastUpdate);
        this.lastUpdate = timestamp;
    }

    // Calculate the screen shake, if we're hurt
    let xShake = 0, yShake = 0;
    let shakeAmplitude = Math.max(0, gameScreenShakeDuration - (timestamp + gameHurtDuration - this.gameHurtEndTime)) / gameScreenShakeDuration;
    if (this.state == GameState.Hurt) {
        let shakeMax = gameScreenShakeMaxPerturbation * shakeAmplitude;

        xShake = (Math.random() <= 0.5 ? 1 : -1) * shakeMax * Math.random();
        yShake = (Math.random() <= 0.5 ? 1 : -1) * shakeMax * Math.random();
    }

    // Update the listener and body positions in the sound engine
    if (this.rissetEngine !== undefined) {
        this.updateRissetEngine(shakeAmplitude);
    }

    // Draw the things~
    for (let i = 0; i < this.gravitationalSystem.allBodies.length; i++) {
        let body = this.gravitationalSystem.allBodies[i];
        context.fillStyle = (i == 0) ? "blue" : "white";
        drawWrappingCircle(context, body.x + xShake, body.y + yShake, body.radius);
    }
    context.fillStyle = "white";
    drawWrappingCircle(context, this.gravitationalSystem.controlPoint.x + this.irisX + xShake, this.gravitationalSystem.controlPoint.y + this.irisY + yShake, 3);

    // Set up for text
    context.save();
    context.globalCompositeOperation = "difference";
    context.fillStyle = "white";

    // Countdown
    if (this.state == GameState.CountingDown || (this.state == GameState.TransitioningOut && this.gameStartTime !== undefined)) {
        let timeLeft = this.gameStartTime - timestamp;
        if (timeLeft <= gameCountdownLength) {
            // Hold the countdown at 1 if we're transitioning out instead of going to 0
            timeLeft = Math.max(1, Math.ceil(timeLeft / 1000));
            context.fillStyle = "white";
            context.font = "bold 60px sans-serif";
            drawCenteredText(context, timeLeft, 65);
        }
    }

    // Timer
    if (this.state == GameState.Playing || this.state == GameState.Hurt || (this.state == GameState.TransitioningOut && this.gameEndTime !== undefined)) {
        // Hold the timer at 0:00 if we're transitioning out instead of going to -1:0-1.
        let totalSecondsLeft = Math.max(0, Math.floor((this.gameEndTime - timestamp) / 1000));
        let minutesLeft = Math.floor(totalSecondsLeft / 60);
        let secondsLeft = totalSecondsLeft % 60;
        if (secondsLeft < 10) {
            secondsLeft = "0" + secondsLeft;
        }
        let timeLeft = minutesLeft + ":" + secondsLeft;

        context.font = "bold 60px sans-serif";
        drawCenteredText(context, timeLeft, 65);
    }

    // Score
    if (defaults.get("freePlay") == "no") {
        context.font = "20px sans-serif";
        context.fillText("Score: " + Math.round(this.score), 10, 465);
        context.fillText("High score: " + defaults.get("highScores")[defaults.get("numberOfBodies") - minBodies] + (this.newHighScore ? " (new!)" : ""), 10, 490);
    }

    // Game over controls
    if (this.state == GameState.GameOver) {
        context.font = "32px sans-serif";
        drawCenteredText(context, "Restart: [Z]", 35);
        drawCenteredText(context, "Quit: [X]", 77);
    }

    context.restore();
}

GameSceneController.prototype.startTransitionIn = function(transitionType) {
    this.state = GameState.TransitioningIn;

    this.gameStartTime = undefined;
    this.gameEndTime = undefined;
    this.gameHurtEndTime = undefined;
    this.gameResetTime = undefined;

    this.gravitationalSystem = new GravitationalSystem(defaults.get("numberOfBodies"), this.updateCallback.bind(this));
    this.originalBodies = this.cloneBodies(this.gravitationalSystem.allBodies);

    if (defaults.get("sound") == "yes") {
        let sources = [];
        for (let i = 0; i < this.gravitationalSystem.bodies.length; i++) {
            let body = this.gravitationalSystem.bodies[i];
            sources.push({
                bottomFrequency: 440 / Math.pow(2, body.mass),
                maxGain: gameBodyMinimumGain + (gameBodyMaximumGain - gameBodyMinimumGain) * (body.mass - bodyMinMass) / (bodyMaxMass - bodyMinMass)
            });
        }
        this.rissetEngine = new RissetEngine(sources);
        this.updateRissetEngine();
    }

    this.score = 0;
    this.newHighScore = false;
    this.irisX = 0;
    this.irisY = 0;
}

GameSceneController.prototype.finishTransitionIn = function(transitionType) {
    this.state = GameState.CountingDown;

    this.gameStartTime = performance.now() + gameCountdownDelay + gameCountdownLength;
}

GameSceneController.prototype.startTransitionOut = function(transitionType) {
    this.state = GameState.TransitioningOut;

    if (this.rissetEngine !== undefined) {
        this.rissetEngine.stop();
        this.rissetEngine = undefined;
    }
}

GameSceneController.prototype.finishTransitionOut = function(transitionType) {
    this.state = GameState.Null;

    this.gravitationalSystem = undefined;
    this.lastUpdate = undefined;
    this.originalBodies = undefined;
    this.bodiesAtReset = undefined;
}

GameSceneController.prototype.pause = function(pauseTime) {
    if (this.rissetEngine !== undefined) {
        this.rissetEngine.mute();
    }
}

GameSceneController.prototype.unpause = function(pauseTime) {
    if (this.rissetEngine !== undefined && (this.state == GameState.Playing || this.state == GameState.FreePlaying || this.state == GameState.Hurt)) {
        this.rissetEngine.unmute();
    }

    if (this.gameStartTime !== undefined) {
        this.gameStartTime += performance.now() - pauseTime;
    }
    if (this.gameEndTime !== undefined) {
        this.gameEndTime += performance.now() - pauseTime;
    }
    if (this.gameHurtEndTime !== undefined) {
        this.gameHurtEndTime += performance.now() - pauseTime;
    }
    if (this.gameResetTime !== undefined) {
        this.gameResetTime += performance.now() - pauseTime;
    }
    if (this.lastUpdate !== undefined) {
        this.lastUpdate += performance.now() - pauseTime;
    }
}

GameSceneController.prototype.keyDown = function(key) {
    if (key == Key.cancel) {
        startTransition(0, Transition.irisOut);
    } else if (key == Key.action) {
        if (this.state == GameState.Playing || this.state == GameState.FreePlaying || this.state == GameState.Hurt || this.state == GameState.GameOver) {
            this.state = GameState.Resetting;

            if (this.rissetEngine !== undefined) {
                this.rissetEngine.mute();
            }

            this.bodiesAtReset = this.cloneBodies(this.gravitationalSystem.allBodies);

            this.score = 0;
            this.newHighScore = false;
            this.irisX = 0;
            this.irisY = 0;

            this.gameEndTime = undefined;
            this.gameHurtEndTime = undefined;
            this.gameResetTime = performance.now() + gameResetDuration;
        }
    }
}

GameSceneController.prototype.keyUp = function(key) {

}
