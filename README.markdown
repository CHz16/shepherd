Shepherd
========

An actiony, score attack game made for [GAMES MADE QUICK??? 1½](https://itch.io/jam/games-made-quick-one-and-a-half). Dedicated to [Roger](https://en.wikipedia.org/wiki/Roger_Shepard) and [Alan](https://en.wikipedia.org/wiki/Alan_Shepard). Play this thing over on the itchio: https://chz.itch.io/shepherd

I'm starting this early because I'm super busy and don't know how much time I'll actually have to work on it during SGDQ. ~~Who knows if I'll finish this!~~ Cheating pays off!

This code is released under MIT (see LICENSE.txt).
