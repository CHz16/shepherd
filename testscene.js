// testscene.js
// Controller for the testing scene.

function TestSceneController() {

    this.rissetEngine = undefined;
    this.sourceX = undefined, this.sourceY = undefined;
    this.listenerX = undefined, this.listenerY = undefined;
}

TestSceneController.prototype.draw = function(context, timestamp) {
    if (!this.transitioning) {
        this.rissetEngine.setScalePositionOfSource(0, ((timestamp - this.startTime) / 10000) % 1);

        let movementVector = {x: 0, y: 0};
        if (keyDown[Key.left]) {
            movementVector.x -= 1;
        }
        if (keyDown[Key.right]) {
            movementVector.x += 1;
        }
        if (keyDown[Key.up]) {
            movementVector.y -= 1;
        }
        if (keyDown[Key.down]) {
            movementVector.y += 1;
        }
        movementVector.x *= 3;
        movementVector.y *= 3;
        if (movementVector.x != 0 && movementVector.y != 0) {
            movementVector.x /= Math.sqrt(2);
            movementVector.y /= Math.sqrt(2);
        }

        if (movementVector.x != 0 || movementVector.y != 0) {
            if (keyDown[Key.action]) {
                this.sourceX = (this.sourceX + movementVector.x) % gameWidth;
                if (this.sourceX < 0) {
                    this.sourceX += gameWidth;
                }
                this.sourceY = (this.sourceY + movementVector.y) % gameHeight;
                if (this.sourceY < 0) {
                    this.sourceY += gameHeight;
                }
                this.rissetEngine.setPositionOfSource(0, this.sourceX, this.sourceY);
            } else {
                this.listenerX = (this.listenerX + movementVector.x) % gameWidth;
                if (this.listenerX < 0) {
                    this.listenerX += gameWidth;
                }
                this.listenerY = (this.listenerY + movementVector.y) % gameWidth;
                if (this.listenerY < 0) {
                    this.listenerY += gameHeight;
                }
                this.rissetEngine.setPositionOfListener(this.listenerX, this.listenerY);
            }
        }
    }

    context.fillStyle = "black";
    context.fillRect(0, 0, gameWidth, gameHeight);

    context.fillStyle = "red";
    context.fillRect(this.sourceX - 10, this.sourceY - 10, 20, 20);
    context.fillStyle = "blue";
    context.beginPath();
    context.arc(this.listenerX, this.listenerY, 10, 0, 360);
    context.closePath();
    context.fill();
}

TestSceneController.prototype.startTransitionIn = function(transitionType) {
    this.transitioning = true;
    this.sourceX = 250, this.sourceY = 225;
    this.listenerX = 250, this.listenerY = 275;
}

TestSceneController.prototype.finishTransitionIn = function(transitionType) {
    this.transitioning = false;
    this.startTime = performance.now();

    this.rissetEngine = new RissetEngine([{x: this.sourceX, y: this.sourceY, bottomFrequency: 440}], {listenerX: this.listenerX, listenerY: this.listenerY});
    this.rissetEngine.unmute();
    //this.rissetEngine.setScalePositionOfSource(0, 0.5);
}

TestSceneController.prototype.startTransitionOut = function(transitionType) {
    this.transitioning = true;
    this.rissetEngine.stop();
    this.rissetEngine = undefined;
}

TestSceneController.prototype.finishTransitionOut = function(transitionType) {
    this.transitioning = false;
}

TestSceneController.prototype.pause = function(pauseTime) {
    // Scene can pause while transitioning, during which the engine won't exist
    if (this.rissetEngine) {
        this.rissetEngine.mute();
    }
}

TestSceneController.prototype.unpause = function(pauseTime) {
    if (this.rissetEngine) {
        this.rissetEngine.unmute();
    }
}

TestSceneController.prototype.keyDown = function(key) {
    if (key == Key.cancel) {
        startTransition(0, Transition.irisOut);
    }
}

TestSceneController.prototype.keyUp = function(key) {

}
