// gravitationalsystem.js
// Manages one or more moving bodies affected by gravity.

var gravitationalConstant = 15;
var maxTimeslice = 20; // milliseconds
var bodyMinMass = 0.5, bodyMaxMass = 3;
var bodyMaxSpeed = 300/1000; // pixels per millisecond
var bodyStartRadius = 100, bodyStartSpeed = 50/1000;

function GravitationalSystem(numberOfBodies, updateCallback) {
    this.updateCallback = updateCallback || function(timeslice, collidedWithControlPoint) { };

    this.controlPoint = {
        x: gameWidth / 2,
        y: gameHeight / 2,
        xVelocity: 0,
        yVelocity: 0,
        mass: 5,
        radius: 10
    };

    // Create n bodies that are equally spaced along a circle around the center of the game, at a randomized angle.
    let startAngle = Math.random() * Math.PI * 2;
    let massStep = (numberOfBodies != 1) ? (bodyMaxMass - bodyMinMass) / (numberOfBodies - 1) : 0;
    let massStart = (numberOfBodies != 1) ? bodyMinMass : (bodyMaxMass + bodyMinMass) / 2;

    this.bodies = [];
    for (let i = 0; i < numberOfBodies; i++) {
        let angle = startAngle + i * (Math.PI * 2 / numberOfBodies);
        let mass = massStart + i * massStep;
        this.bodies.push({
            x: gameWidth / 2 + Math.cos(angle) * bodyStartRadius,
            y: gameHeight / 2 + Math.sin(angle) * bodyStartRadius,
            xVelocity: Math.cos(angle) * bodyStartSpeed,
            yVelocity: Math.sin(angle) * bodyStartSpeed,
            mass: mass,
            radius: 20 * Math.sqrt(mass)
        });
    }

    // Make a combined array containing both the control point and all bodies. Simplifies a couple of things.
    this.allBodies = [this.controlPoint].concat(this.bodies);
}

// Advance the gravitational system by a given number of milliseconds.
GravitationalSystem.prototype.update = function(dt) {
    // Slice dt up into maxTimeslice sized chunks, in case we end up running slowly
    while (dt > 0) {
        let slice = Math.min(dt, maxTimeslice);
        dt -= maxTimeslice;

        // Do everything that changes a body's position first before touching velocities

        // Move everything according to their current velocities
        // The control point only moves when the player tells it to so loop over this.bodies
        for (let i = 0; i < this.bodies.length; i++) {
            let body = this.bodies[i];
            body.x = (body.x + body.xVelocity * slice) % gameWidth;
            if (body.x < 0) {
                body.x += gameWidth;
            }
            body.y = (body.y + body.yVelocity * slice) % gameHeight;
            if (body.y < 0) {
                body.y += gameHeight;
            }
        }

        // If two bodies are overlapping or touching, then back them up along their tangent line until they're not and marked that they collided for later.
        // Since we only make one pass through each pair, it's possible that the nudging will then cause a body to overlap another body, but hopefully that will then be caught and fixed during the next update() pass? Maybe? Watch as this causes cycles of three or more objects to repeatedly clip into one another lmao
        // We want to check collisions with the control point so loop over this.allBodies
        let collisions = [];
        let collidedWithControlPoint = false;
        for (let i = 0; i < this.allBodies.length - 1; i++) {
            for (let j = i + 1; j < this.allBodies.length; j++) {
                let body1 = this.allBodies[i];
                let body2 = this.allBodies[j];
                let virtualBodies = this.makeVirtualBodies(body1, body2);
                let virtualBody1 = virtualBodies.virtualBody1;
                let virtualBody2 = virtualBodies.virtualBody2;

                let d = distanceBetweenPoints(body1.x, body1.y, virtualBody2.x, virtualBody2.y);
                if (d >= (body1.radius + body2.radius)) {
                    continue;
                }
                collisions.push([i, j]);

                let normalVectorX = (virtualBody2.x - body1.x) / d;
                let normalVectorY = (virtualBody2.y - body1.y) / d;

                let nudgeDistance = body1.radius + body2.radius - d;
                if (i != 0) {
                    body1.x -= (nudgeDistance / 2) * normalVectorX;
                    body1.y -= (nudgeDistance / 2) * normalVectorY;
                    body2.x += (nudgeDistance / 2) * normalVectorX;
                    body2.y += (nudgeDistance / 2) * normalVectorY;
                } else {
                    // The control point (i == 0) will only move when the player moves it, so we'll eject the second body all the way
                    body2.x += nudgeDistance * normalVectorX;
                    body2.y += nudgeDistance * normalVectorY;
                    collidedWithControlPoint = true;
                }
            }
        }

        // Now work on velocities without touching positions

        // Apply gravity
        // The gravity from the control point affects the other bodies, so we loop over this.allBodies to generate all pairs
        for (let i = 0; i < this.allBodies.length - 1; i++) {
            for (let j = i + 1; j < this.allBodies.length; j++) {
                let body1 = this.allBodies[i];
                let body2 = this.allBodies[j];
                let virtualBodies = this.makeVirtualBodies(body1, body2);
                let virtualBody1 = virtualBodies.virtualBody1;
                let virtualBody2 = virtualBodies.virtualBody2;

                let r2 = distanceSquared(body1.x, body1.y, virtualBody2.x, virtualBody2.y);
                let r = Math.sqrt(r2);

                // The control point (i == 0) will not have any gravitational forces act on it
                if (i != 0) {
                    let f1 = gravitationalConstant * body2.mass / r2;
                    body1.xVelocity += f1 * (virtualBody2.x - body1.x) / r;
                    body1.yVelocity += f1 * (virtualBody2.y - body1.y) / r;
                }

                let f2 = gravitationalConstant * body1.mass / r2;
                body2.xVelocity += f2 * (virtualBody1.x - body2.x) / r;
                body2.yVelocity += f2 * (virtualBody1.y - body2.y) / r;
            }
        }

        // Check the collisions and resolve them
        // The collision checker indexes into this.allBodies, so this has to also
        for (let i = 0; i < collisions.length; i++) {
            let body1 = this.allBodies[collisions[i][0]];
            let body2 = this.allBodies[collisions[i][1]];
            let virtualBodies = this.makeVirtualBodies(body1, body2);
            let virtualBody1 = virtualBodies.virtualBody1;
            let virtualBody2 = virtualBodies.virtualBody2;

            if (collisions[i][0] == 0) {
                // If one of the bodies is the control point, then return all momentum to the second body and reflect it
                let d = distanceBetweenPoints(virtualBody1.x, virtualBody1.y, body2.x, body2.y);
                let normalVectorX = (body2.x - virtualBody1.x) / d;
                let normalVectorY = (body2.y - virtualBody1.y) / d;

                let dot = normalVectorX * body2.xVelocity + normalVectorY * body2.yVelocity;
                body2.xVelocity -= 2 * dot * normalVectorX;
                body2.yVelocity -= 2 * dot * normalVectorY;
            } else {
                // Otherwise, resolve the collision elastically
                let r2 = distanceSquared(body1.x, body1.y, virtualBody2.x, virtualBody2.y);
                // There's a lot of duplicated math here in scale1 and scale2, maybe I'll refactor this later (which means I won't)
                let scale1 = 2 * body2.mass / (body1.mass + body2.mass) * ((body1.xVelocity - body2.xVelocity) * (body1.x - virtualBody2.x) + (body1.yVelocity - body2.yVelocity) * (body1.y - virtualBody2.y)) / r2;
                let scale2 = 2 * body1.mass / (body1.mass + body2.mass) * ((body2.xVelocity - body1.xVelocity) * (body2.x - virtualBody1.x) + (body2.yVelocity - body1.yVelocity) * (body2.y - virtualBody1.y)) / r2;

                body1.xVelocity -= scale1 * (body1.x - virtualBody2.x);
                body1.yVelocity -= scale1 * (body1.y - virtualBody2.y);
                body2.xVelocity -= scale2 * (body2.x - virtualBody1.x);
                body2.yVelocity -= scale2 * (body2.y - virtualBody1.y);
            }
        }

        // Cap the speeds of everything
        // The control point won't ever have a velocity because the player moves it, so loop over this.bodies
        for (let i = 0; i < this.bodies.length; i++) {
            let body = this.bodies[i];
            let speed = Math.sqrt(body.xVelocity * body.xVelocity + body.yVelocity * body.yVelocity);
            if (speed > bodyMaxSpeed) {
                body.xVelocity *= bodyMaxSpeed / speed;
                body.yVelocity *= bodyMaxSpeed / speed;
            }
        }

        this.updateCallback(slice, collidedWithControlPoint);
    }
}

// Change the number of bodies in the system.
// If reducing the number, this will trim bodies from the end of this.bodies.
// If increasing the number, this will add bodies at random locations with random parameters.
GravitationalSystem.prototype.setNumberOfBodies = function(numberOfBodies) {
    if (numberOfBodies < this.bodies.length) {
        this.bodies.splice(numberOfBodies);
        this.allBodies.splice(numberOfBodies + 1);
    } else {
        for (let i = 0; i < (numberOfBodies - this.bodies.length); i++) {
            let mass = Math.random() * 2.75 + 0.25;
            let body = {
                x: gameWidth * Math.random(),
                y: gameHeight * Math.random(),
                xVelocity: bodyMaxSpeed / 2 * Math.random(),
                yVelocity: bodyMaxSpeed / 2 * Math.random(),
                mass: mass,
                radius: 20 * Math.sqrt(mass)
            };

            this.bodies.push(body);
            this.allBodies.push(body);
        }
    }
}

// Since the world is a torus, we need to take into account that there may be a shorter path between two bodies by screen wrapping. We'll do it by creating a "virtual" bodies offscreen that lie where the force on a body should be coming from. This function returns a struct containing two virtual bodies, one for each of the arguments with regard to the other argument.
// This is public because I use this in the game scene too lmao what even is encapsulation
GravitationalSystem.prototype.makeVirtualBodies = function(body1, body2) {
    let virtualBody1 = Object.assign({}, body1);
    let virtualBody2 = Object.assign({}, body2);
    if ((body2.x + gameWidth / 2) < body1.x) {
        virtualBody1.x -= gameWidth;
        virtualBody2.x += gameWidth;
    } else if ((body2.x - gameWidth / 2) > body1.x) {
        virtualBody1.x += gameWidth;
        virtualBody2.x -= gameWidth;
    }
    if ((body2.y + gameHeight / 2) < body1.y) {
        virtualBody1.y -= gameHeight;
        virtualBody2.y += gameHeight;
    } else if ((body2.y - gameHeight / 2) > body1.y) {
        virtualBody1.y += gameHeight;
        virtualBody2.y -= gameHeight;
    }
    return {virtualBody1: virtualBody1, virtualBody2: virtualBody2};
}
