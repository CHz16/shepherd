// titlescene.js
// Controller for the title scene.

function TitleSceneController() {

    this.selectedMenuItem = 0;

    this.gravitationalSystem = undefined;
    this.lastUpdate = undefined;
}

TitleSceneController.prototype.draw = function(context, timestamp) {
    // Update the bodies
    this.gravitationalSystem.update(timestamp - this.lastUpdate);
    this.lastUpdate = timestamp;

    // Draw the title
    context.fillStyle = "black";
    context.font = "60px sans-serif";
    drawCenteredText(context, "Shepherd", 65);

    // Draw the menu
    let numberOfBodies = defaults.get("numberOfBodies");
    let menu = [
        "Bodies: " + numberOfBodies,
        "High score: " + defaults.get("highScores")[numberOfBodies - minBodies],
        "Free play: " + defaults.get("freePlay"),
        "Sound: " + defaults.get("sound")
    ];
    if (defaults.get("freePlay") == "yes") {
        menu[1] = "";
    }
    let fontSizes = [32, 16, 32, 32];
    let lineHeights = [22, 29, 40];
    let y = 390;
    for (let i = 0; i < menu.length; i++) {
        let font = fontSizes[i] + "px sans-serif";
        if (i == this.selectedMenuItem) {
            font = "bold " + font;
        }
        context.font = font;
        drawCenteredText(context, menu[i], y);
        y += lineHeights[i];
    }

    // Draw the control point
    context.fillStyle = "blue";
    drawWrappingCircle(context, this.gravitationalSystem.controlPoint.x, this.gravitationalSystem.controlPoint.y, this.gravitationalSystem.controlPoint.radius);
    context.fillStyle = "white";
    drawWrappingCircle(context, this.gravitationalSystem.controlPoint.x, this.gravitationalSystem.controlPoint.y, 3);

    // Set up for drawing the bodies
    // The difference compositing looks bad on black-on-white text in Firefox and Chrome, so we
    // do it on the bodies here instead
    // Maybe I should backport this to the game scene too??
    context.save();
    context.globalCompositeOperation = "difference";
    context.fillStyle = "white";

    // Draw the bodies
    for (let i = 0; i < this.gravitationalSystem.bodies.length; i++) {
        let body = this.gravitationalSystem.bodies[i];
        drawWrappingCircle(context, body.x, body.y, body.radius);
    }

    context.restore();
}

TitleSceneController.prototype.startTransitionIn = function(transitionType) {
    this.selectedMenuItem = 0;
    this.gravitationalSystem = new GravitationalSystem(defaults.get("numberOfBodies"));
}

TitleSceneController.prototype.finishTransitionIn = function(transitionType) {
    this.lastUpdate = performance.now();
}

TitleSceneController.prototype.startTransitionOut = function(transitionType) {

}

TitleSceneController.prototype.finishTransitionOut = function(transitionType) {
    this.gravitationalSystem = undefined;
    this.lastUpdate = undefined;
}

TitleSceneController.prototype.pause = function(pauseTime) {

}

TitleSceneController.prototype.unpause = function(pauseTime) {
    this.lastUpdate += performance.now() - pauseTime;
}

TitleSceneController.prototype.keyDown = function(key) {
    if (key == Key.action) {
        startTransition(1, Transition.irisIn);
    } else if (key == Key.up) {
        this.selectedMenuItem = (this.selectedMenuItem + 3) % 4;
        // Skip the high score line
        if (this.selectedMenuItem == 1) {
            this.selectedMenuItem = 0;
        }
    } else if (key == Key.down) {
        this.selectedMenuItem = (this.selectedMenuItem + 1) % 4;
        // Skip the high score line
        if (this.selectedMenuItem == 1) {
            this.selectedMenuItem = 2;
        }
    } else if (key == Key.left || key == Key.right) {
        if (this.selectedMenuItem == 0) {
            let numberOfBodies = defaults.get("numberOfBodies");
            if (key == Key.left) {
                numberOfBodies = Math.max(numberOfBodies - 1, minBodies);
            } else {
                numberOfBodies = Math.min(numberOfBodies + 1, maxBodies);
            }
            this.gravitationalSystem.setNumberOfBodies(numberOfBodies);
            defaults.set("numberOfBodies", numberOfBodies);
        } else if (this.selectedMenuItem == 2) {
            if (defaults.get("freePlay") == "yes") {
                defaults.set("freePlay", "no");
            } else {
                defaults.set("freePlay", "yes");
            }
        } else if (this.selectedMenuItem == 3) {
            if (defaults.get("sound") == "yes") {
                defaults.set("sound", "no");
            } else {
                defaults.set("sound", "yes");
            }
        }
    }
}

TitleSceneController.prototype.keyUp = function(key) {

}
